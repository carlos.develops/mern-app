const withCSS = require('@zeit/next-css')
const withSass = require('@zeit/next-sass')
const withImages = require('next-images')
module.exports = withCSS();
module.exports = withSass({ cssModules: true });
module.exports = withImages({});

module.exports = {
    exportTrailingslash: true,
    exportPathMap: function() {
        const path = {
            "/": {page: "/"},
            "/blog": {page: "/blog"},
            "/contact": {page: "/contact"},
            "/admin": {page: "/admin"}
        };

        return path;
    },
    experimental: {
        css: true
    }
}