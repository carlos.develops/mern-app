const mongoose = require('../database');
const {Schema} = mongoose;

const MetaTagSchema = new Schema({
    name: {type: String, require: true},
    content: {type: String, require: true},
})

module.exports = mongoose.model( 'MetaTag', MetaTagSchema );