const mongoose = require('../database');
const {Schema} = mongoose;

// mongoose.set('useFindAndModify', false);

const SectionSchema = new Schema({
    title: {type: String, require: true},
    description: {type: String, require: true},
})

module.exports = mongoose.model( 'Section', SectionSchema );