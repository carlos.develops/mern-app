const mongoose = require('mongoose');
const URI = 'mongodb://localhost/admin-db';

mongoose.connect(URI, { useFindAndModify: false })
        .then(db => console.log('DB is connected'))
        .catch(err => console.log(err));


module.exports = mongoose;