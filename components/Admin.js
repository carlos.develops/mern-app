import React, {Component} from 'react';

import AdminSection from './AdminSection';
import AdminMetaTags from './AdminMetaTags';

class Admin extends Component {
    render() {
        return (
            <div>
                <AdminSection />
                <AdminMetaTags />
            </div>
        );
    };
};

export default Admin;