import React, {Component} from 'react';
import Header from './Header';
import Footer from './Footer';
import MetaTags from '../components/MetaTags';

const Page = props => (
    <div>
		<MetaTags/>
		<Header />

		{props.children}

		<Footer />
    </div>
);

export default Page;