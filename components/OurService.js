import React, {Component} from "react";

class OurService extends Component {

    constructor() {
        super();

        this.state = {
            _id: '',
            title: '',
            description: '',
            sections: [],
        };
    }

    componentDidMount() {
		this.fetchSection();
		console.log(this.state.sections[1]);
    }

    fetchSection() {
        fetch('/admin/sections')
        .then(res => res.json())
        .then(data => {
            console.log(data[1]._id);
            this.setState({
				_id: data[1]._id,
				title: data[1].title,
				description: data[1].description
			});
        });
    }

    render() {
        return (
            <section className="container-fluid my-5 pt-5">
                <div className="row align-items-end">
					<div className="col-12 col-md-4">
						<img src="https://via.placeholder.com/600x635.png/000000/ffffff/?text=Placeholder" className="img-fluid" />
					</div>
					<div className="col-12 col-md-3">
						<h2>{ this.state.title }</h2>
						{/* <h2>Nuestros Servicios</h2> */}
						<p> { this.state.description } </p>
						{/* <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat.
						</p> */}
						<img src="https://via.placeholder.com/450x395.png/000000/ffffff/?text=Placeholder" className="img-fluid" />
					</div>
					<div className="col-12 col-md-5">
						<img src="https://via.placeholder.com/600x500.png/000000/ffffff/?text=Placeholder" className="img-fluid" />
					</div>
				</div>
            </section>
        )
    }
}

export default OurService;

