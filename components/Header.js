import React, {Component} from 'react';
import Link from "next/link";

class Header extends Component {
    render(){
        return (
            <div className="container-fluid">
                <nav className="navbar navbar-expand-lg">
                    <a className="navbar-brand" href="#">
                        <img src="https://via.placeholder.com/120x30.png/000000/ffffff/?text=Logo" alt="Logo" />
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item active">
                                <Link href="/admin">
                                    <a className="nav-link"> <b>(Admin ultra secreto!)</b> </a>
                                </Link>
                            </li>
                            <li className="nav-item active">
                                <Link href="/">
                                    <a className="nav-link">Home</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/blog">
                                    <a className="nav-link">Acerca de</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/blog">
                                    <a className="nav-link">Servicios</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/">
                                    <a className="nav-link">Desarrollos</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/">
                                    <a className="nav-link">Blog</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/">
                                    <a className="nav-link">Contacto</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/">
                                    <a href="#" className="btn btn-outline-warning">¿Necesitas asesoría?</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <style jsx>{`
                    `}</style>
                </nav>
            </div>
        )
    }
}

export default Header;