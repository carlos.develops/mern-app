import React, {Component} from 'react';
import Head from 'next/head';
// import { render } from 'node-sass';



class MetaTags extends Component {

    constructor() {
        super();

        this.state = {
            _id: '',
            content: '',
            description: '',
            tags: [],
        };
    }

    componentDidMount() {
		this.fetchTags();
    }

    fetchTags() {
        fetch('/admin/metatags')
        .then(res => res.json())
        .then(data => {
            this.setState({
				tags: data,
			});
        });
    }

    render() {
        return (
            <Head>
                <title> chales! </title>
                <meta name="description" content="Para alguien que hace PHP ésto es un complicado" />
                <meta name="author" content="Nombre del autor" />
            </Head>
        )
    }
}

export default MetaTags;