import React, {Component} from 'react';

class Admin extends Component {

    constructor() {
        super();

        this.state = {
            _id: '',
            title: '',
            description: '',
            sections: []
        };
        this.addSection = this.addSection.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        console.log("==== componentDidMount ====");
        this.fetchSections();
    }

    fetchSections() {
        console.log("==== fetchSections ====");
        fetch('/admin/sections')
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            this.setState({sections: data});
            console.log(this.state.sections);
        });
    }

    addSection( e ) {
        console.log("==== addSection ====");
        console.log(this.state);

        if( this.state._id ) {
            // Update Section:
            console.log("Update Section");
            fetch(`/admin/sections/${this.state._id}`, {
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // M.toast({html: 'Section Updated!'});
                // Clean state:
                this.setState({title: '', description: '', _id: ''});
                // Fetch Data:
                this.fetchSections();
            })
            .catch(err => console.log(err));
        } else {
            // Save Section
            console.log("Save Section");
            fetch('/admin/sections', {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log("==== addSection ====");
                console.log(data);
                // M.toast({html: 'Section Saved'});
                // Clean state:
                this.setState({title: '', description: ''});
                // Fetch Data:
                this.fetchSections();
            })
            .catch(err => console.log(err));
        }

        e.preventDefault();

    }

    editSection(id) {
        console.log("editing! " + id);
        fetch(`admin/sections/${id}`)
        .then(res => res.json())
        .then(data => {
            console.log("data: ");
            console.log(data._id);
            console.log(data.title);
            console.log(data.description);
            this.setState({
                _id: data._id,
                title: data.title ,
                description: data.description
            });
            // M.toast({html: 'Task Update!'});
            console.log("Section updated!");
            this.fetchSections();
        });
    }

    deleteSection(id) {

        // console.log(`Deleting ${id}`);

        if(confirm('Are you sure you want to delete it?')) {
            console.log("deleting! " + id);
            fetch(`admin/sections/${id}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // M.toast({html: 'Section Deleted!'});
                this.fetchSections();
            });
        }
    }

    handleChange( e ) {
        console.log("==== handleChange ====");
        console.log(e.target.value);
        console.log(e.target.name);
        const {name, value} = e.target;
        this.setState({
            [name]: value
        });
    }

    render() {
        return (
            <div className="row m-5">
                <div className="col-12 text-center my-4">
                    <h2>Administrar Secciones (Acerca de / Nuestros servicios)</h2>
                </div>
                <div className="col-md-5">
                    <div className="card">
                        <div className="card-body">
                            <form onSubmit={ this.addSection }>
                                <div className="row">
                                    <div className="form-group">
                                        <input name="title" onChange={ this.handleChange } className="form-control" type="text" placeholder="section title" value={this.state.title} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="form-group">
                                        <textarea name="description" onChange={ this.handleChange } className="form-control" placeholder="section description" value={this.state.description}></textarea>
                                    </div>
                                </div>
                                <button type="submit" className="btn btn-primary">
                                    Guardar
                                </button>
                            </form>
                        </div>
                    </div>
                </div>

                <div className="col-md-7">
                    <table className="table table-dark">
                        <thead className="thead-dark">
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>{
                            this.state.sections.map(section => {
                                return (
                                    <tr key={section._id}>
                                        <td>{section.title}</td>
                                        <td>{section.description}</td>
                                        <td>
                                            <button className="btn btn-warning" onClick={ () => this.editSection(section._id) }>edit</button>
                                            <button className="btn btn-danger" onClick={ () => this.deleteSection(section._id) }>
                                                delete
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })
                        }</tbody>
                    </table>
                </div>
            </div>
        );
    };
};

export default Admin;