import React, {Component} from "react";

class AboutUs extends Component {

    constructor() {
        super();

        this.state = {
            _id: '',
            title: '',
            description: '',
            sections: [],
        };
    }

    componentDidMount() {
		this.fetchSection();
		console.log(this.state.sections[0]);
    }

    fetchSection() {
        fetch('/admin/sections')
        .then(res => res.json())
        .then(data => {
            console.log(data[0]._id);
            this.setState({
				_id: data[0]._id,
				title: data[0].title,
				description: data[0].description
			});
        });
    }

    render() {
        return (
            <section className="container-fluid my-5 pt-5">
                <div className="row">
                    <div className="col-12 col-md-6 text-center">
                        <img src="https://via.placeholder.com/400x300.png/000000/ffffff/?text=Placeholder" className="img-fluid"/>
                    </div>
                    <div className="col-12 col-md-6">
                        <div className="row">
                            <div className="col-12">
                                <h2>{ this.state.title }</h2>
                                {/* <h2>Acerca de nosotros!</h2> */}
                            </div>
                            <div className="col-10">
                                <p>{ this.state.description }</p>
                                {/* <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> */}
                                <a href="" className="btn btn-primary">Contáctanos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default AboutUs;

