import React, {Component} from 'react';

class AdminMetaTags extends Component {
    /*
    <title> Guaostudio || challenge :/ </title>
    <meta name="content" content="Para alguien que hace PHP ésto es un complicado" />
    <meta name="author" content="Nombre del autor" />
    */
    constructor() {
        super();

        this.state = {
            _id: '',
            name: '',
            content: '',
            tags: []
        };
        this.addMeta = this.addMeta.bind(this);
        this.handleChangeField = this.handleChangeField.bind(this);
    }

    componentDidMount() {
        console.log("==== componentDidMount MetaTags ====");
        this.fetchMetas();
    }

    fetchMetas() {
        console.log("==== fetchMetas ====");
        fetch('/admin/metatags')
        .then(res => res.json())
        .then(data => {
            console.log(data);
            this.setState({tags: data});
            console.log(this.state.tags);
        });
    }

    addMeta( e ) {
        console.log("==== addMeta ====");
        console.log(this.state);

        if( this.state._id ) {
            // Update Meta:
            console.log("Update Meta");
            fetch(`/admin/metatags/${this.state._id}`, {
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // Clean state:
                this.setState({name: '', content: '', _id: ''});
                // Fetch Data:
                this.fetchMetas();
            })
            .catch(err => console.log(err));
        } else {
            // Save Meta
            console.log("Save Meta");
            console.log(JSON.stringify(this.state));
            fetch('/admin/metatags', {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log("==== addMeta ====");
                console.log(data);
                // Clean state:
                this.setState({name: '', content: ''});
                // Fetch Data:
                this.fetchMetas();
            })
            .catch(err => console.log(err));
        }

        e.preventDefault();

    }

    editMeta(id) {
        console.log("for edit Meta: " + id);
        fetch(`admin/metatags/${id}`)
        .then(res => res.json())
        .then(data => {
            console.log("data: ");
            console.log(data._id);
            console.log(data.name);
            console.log(data.content);
            this.setState({
                _id: data._id,
                name: data.name ,
                content: data.content
            });
            this.fetchMetas();
        });
    }

    deleteMeta(id) {
        console.log(`Deleting ${id}`);
        if(confirm('Are you sure you want to delete it?')) {
            console.log("deleting! " + id);
            fetch(`admin/metatags/${id}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // M.toast({html: 'Meta Deleted!'});
                this.fetchMetas();
            });
        }
    }

    handleChangeField( e ) {
        console.log("==== handleChangeField ====");
        console.log(e.target.value);
        console.log(e.target.name);
        const {name, value} = e.target;
        this.setState({
            [name]: value
        });
    }

    render() {
        return (
            <div className="row m-5">
                <div className="col-12 text-center my-4">
                    <h2>Administrar Tags (name / content)</h2>
                </div>
                <div className="col-md-5">
                    <div className="card">
                        <div className="card-body">
                            <form onSubmit={ this.addMeta }>
                                <div className="row">
                                    <div className="form-group">
                                        <input name="name" className="form-control" onChange={ this.handleChangeField } type="text" placeholder="meta name" value={ this.state.name } />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="form-group">
                                        <textarea name="content" className="form-control" onChange={ this.handleChangeField } placeholder="meta content" value={ this.state.content }></textarea>
                                    </div>
                                </div>
                                <button type="submit" className="btn btn-primary">
                                    Guardar
                                </button>
                            </form>
                        </div>
                    </div>
                </div>

                <div className="col-md-7">
                    <table className="table table-dark">
                        <thead className="thead-dark">
                            <tr>
                                <th>Name Tag</th>
                                <th>content</th>
                            </tr>
                        </thead>
                        <tbody>{
                            this.state.tags.map(tag => {
                                return (
                                    <tr key={tag._id}>
                                        <td>{tag.name}</td>
                                        <td>{tag.content}</td>
                                        <td>
                                            <button className="btn btn-warning" onClick={ () => this.editMeta(tag._id) }>edit</button>
                                            <button className="btn btn-danger" onClick={ () => this.deleteMeta(tag._id) }>
                                                delete
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })
                        }</tbody>
                    </table>
                </div>
            </div>
        );
    };
};

export default AdminMetaTags;