/*

Con éste tutorial aprendí a configurar-ejecutar (otra vez) un proyecto por medio de npm:


Los scripts para los comandos son:


    "dev": "next",
    "build": "next build",
    "star": "next star",
    "export": "next export",
    la siguiente forma, ejecuta build y export en un solo comando, además crea una carpeta llamada build
    con todos los estáticos:
    "export": "next build && next export -o build"

    comandos:
    npm run-script dev      -       ejecuta el servidor
    npm run-script build      -     mmm.. ¿compila el proyecto?
    npm run-script export      -     mmmm... crea los archivos státicos, por default una carpeta
                                    en una carpeta llamada: out
*/
