import React from "react";
import fetch from 'isomorphic-unfetch';

import Page from '../components/Page';
import Carousel from '../components/Carousel';
import AboutUs from '../components/AboutUs';
import OurService from '../components/OurService';
import Footer from '../components/Footer';

// Intenté importar imágenes, hasta instalé un next-images, pero nada.
// import logo from '../public/ara-logo.png';
// import Background from '../images/bg.jpg';

const index = ( props ) => {

    // console.log( 'props: ');
    // console.log(  props );

    // const {posts} = props;

    return (
        <Page>
            <Carousel />
            <AboutUs />
            <OurService />
            {/* ésto es lo que se refleja en props.children */}
            {/* Hello World! - homePage. */}

            {/* Probando con la api de jsonplaceholder */}
            {/* <div className="container-fluid">
                <div className="row">
                        { posts.map( post => (
                        <div className="col-4" key={post.id}>
                                <p>{post.title}</p>
                        </div>
                        )) }
                </div>
            </div> */}
        </Page>
    );
}

// index.getInitialProps = async () => {
//     const res = await fetch( 'https://jsonplaceholder.typicode.com/posts' );
//     const data = await res.json()
//     return { data }; // <-- set whatever key you want.
// };

// This function gets called at build time
// export async function getStaticProps() {
//     // Call an external API endpoint to get posts
//     // const res = await fetch('https://jsonplaceholder.typicode.com/posts/10')
//     const res = await fetch('http://localhost:3000/admin/metatags')
//     const tags = await res.json()
//     console.log("tags: ");
//     console.log(tags);
//     // By returning { props: posts }, the Blog component
//     // will receive `posts` as a prop at build time
//     return {
//       props: {
//         tags,
//       },
//     }
// }



export default index;