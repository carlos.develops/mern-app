import React from "react";
import Page from '../components/Page';
import Admin from '../components/Admin';

export default function admin() {
    return (
        <Page>
            <Admin />
        </Page>
    );
}