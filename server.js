const express = require('express')
const next = require('next')
const bodyParser = require('body-parser')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const Section = require('./models/section');
const MetaTag = require('./models/metatags');

app.prepare().then(() => {
    const server = express();
    server.use(bodyParser.urlencoded({ extended: true }));
	server.use(bodyParser.json());

	// ==== SECTIONS ====
    server.get('/admin/sections', async (req, res) => {
        const sections = await Section.find(); // Find All
        res.json(sections);
    });
    // ==== Get Section ====
    server.get('/admin/sections/:id', async (req, res) => {
        const section = await Section.findById(req.params.id);
        res.json(section);
    });
    // Save a new section
    server.post('/admin/sections', async (req, res) => {
        const {title, description} = req.body;
        const section = new Section({title, description});
        await section.save();

        res.json({status: 'Section Saved'});
    });
    // Update section
    server.put('/admin/sections/:id', async (req, res) => {
        const {title, description} = req.body;
        const newSection = {title, description};
                    //   findOneAndUpdate
        await Section.findByIdAndUpdate(req.params.id, newSection);
        res.json({status: 'Section Updated'});
    });
    // Delete Section
    server.delete('/admin/sections/:id', async (req, res) =>  {
        await Section.findByIdAndRemove(req.params.id);
        res.json({status: 'Section Removed'});
    });

	// ==== METATAGS ====
    server.get('/admin/metatags', async (req, res) => {
        const tags = await MetaTag.find(); // Find All
        res.json(tags);
    });
    // ==== Get Tags ====
    server.get('/admin/metatags/:id', async (req, res) => {
        const tag = await MetaTag.findById(req.params.id);
        res.json(tag);
    });
    // Save a new Tag
    server.post('/admin/metatags', async (req, res) => {
        const {name, content} = req.body;
        const tag = new MetaTag({name, content});
        await tag.save();

        res.json({status: 'Tag Saved'});
    });
    // Update Tag
    server.put('/admin/metatags/:id', async (req, res) => {
        const {name, content} = req.body;
        const newTag = {name, content};
        await MetaTag.findByIdAndUpdate(req.params.id, newTag);
        res.json({status: 'Section Updated'});
    });
    // Delete Tag
    server.delete('/admin/metatags/:id', async (req, res) =>  {
        await MetaTag.findByIdAndRemove(req.params.id);
        res.json({status: 'Section Removed'});
    });

	server.all('*', (req, res) => {
		return handle(req, res)
	})

	server.listen(port, err => {
		if (err) throw err
		console.log(`> Ready on http://localhost:${port}`)
	})
})